#!/bin/bash
#A script to renew a Let's Encrypt certificate without any mucking about
#Use the standalone webserver to get a new certificate, then copy it over the old one
#wherever the webserver (nginx) looks for it

#Stop the nginx server so Let's Encrypt can hog port 80
service nginx stop

#The domain to be renewed
echo "The FQDN of the domain to be renewed:"
read -e domain
#Where the config file is
#All you need in the config file is domains, email and rsa key size
echo "The path to the config file:"
read -e config
#config=/home/user/my.letsencrypt.config.ini
#Where to copy the keys
ssldir=/etc/nginx/ssl
#Today's date
today=`date "+%Y%m%d"`

#Renew the cert
/opt/letsencrypt/letsencrypt-auto certonly --standalone --renew-by-default --agree-tos --config $config

#Rename the old cert
mv /etc/nginx/ssl/$domain.key /etc/nginx/ssl/$domain.key.old.$today
mv /etc/nginx/ssl/$domain.crt /etc/nginx/ssl/$domain.crt.old.$today

#Copy the new cert over
cp /etc/letsencrypt/live/$domain/privkey.pem /etc/nginx/ssl/$domain.key
cp /etc/letsencrypt/live/$domain/fullchain.pem /etc/nginx/ssl/$domain.crt

#Set permissions on key files, root readable only
chmod 400 /etc/nginx/ssl/$domain.key*

#Set permissions on certificate files
chmod 444 /etc/nginx/ssl/$domain.crt*

#Start the webserver back up
service nginx start

#Once tested, be sure to revoke your old scripts with
#/opt/letsencrypt/letsencrypt-auto revoke --cert-path /etc/nginx/ssl/oldcert.fullchain.crt

#Sample my.letsencrypt.config.ini:
#rsa-key-size = 4096
#email = admin@mydomain.com
#domains = mydomain.com, www.mydomain.com
