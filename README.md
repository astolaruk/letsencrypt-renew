# letsencrypt-renew
I wrote this script to assist with renewal of Let's Encrypt certificates. I no longer use this as Let's Encrypt is now available in major repositories.

It uses the --standalone option, so any webserver running on port 80 will need to be stopped.

If you can install Let's Encrypt from your distribution's repositories and run it that way, I highly recommend that over this script.
